.. _opnfv-user-config:

.. This work is licensed under a Creative Commons Attribution 4.0 International License.
.. SPDX-License-Identifier: CC-BY-4.0
.. (c) Anuket CCC, AT&T, and other contributors

================================
User Guide & Configuration Guide
================================

Abstract
========

One of Anuket's project goals is to reduce time to integrate and deploy NFV infrastructure and onboard VNF/CNFs
for those who supply components and those who operationalize these platforms. The Anuket community
does this by implementing, testing and deploying tools for conformance and performance of NFV infrastructure, aligned
with industry reference architectures. This document provides guidance and instructions for using platform
features designed to support the tools that are made available in the Anuket
Kali release.

This document is not intended to replace or replicate documentation from other
upstream open source projects such as KVM, OpenDaylight, OpenStack, etc., but to highlight the
features and capabilities delivered through the Anuket project.


Introduction
============

Anuket provides a infrastructure deployment options, which
are able to be installed to host virtualised network functions (VNFs).
This document intends to help users of the platform leverage the features and
capabilities delivered by Anuket.

Feature Overview
================

The following links outline the feature deliverables from participating Anuket
projects in the Kali release. Each of the participating projects provides
detailed descriptions about the delivered features including use cases,
implementation, and configuration specifics in the project documentation.

The following Configuration Guides and User Guides assume that the reader already has some
knowledge about a given project's specifics and deliverables. These Guides
are intended to be used following the installation with an Anuket installer
to allow users to deploy and implement feature delivered by Anuket.

If you are unsure about the specifics of a given project, please refer to the
Anuket wiki page at http://wiki.opnfv.org for more details.


Feature Configuration Guides
============================

- :ref:`Barometer Configuration Guide <barometer-configguide>`
- :ref:`CIRV Configuration Guide <cirv-configguide>`


Feature User Guides
===================

- :ref:`Barometer User Guide <barometer-userguide>`
- :ref:`CIRV User Guide <cirv-userguide>`

*   :doc:`ViNePERF Configuration and User Guide <vineperf:testing/user/configguide/index>`

